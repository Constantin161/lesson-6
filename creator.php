<?php


session_start();

if (!isset($_SESSION['id_status']) || $_SESSION['id_status'] != 1)
{
	http_response_code(403);
	echo "У вас нет прав, для выполнения этого действия!";
	die;
}

//Если форма не загружалась выводим её
if (!isset($_POST['question']) && !isset($_POST['answer'])) : ?>

<form method="post" action="creator.php">
	<h2> Новый вопрос </h2>
	<textarea required cols="40" rows="3" name="question" placeholder="Введите правильный ответ"></textarea>
	<h2> Правильный ответ </h2>
	<textarea required cols="40" rows="3" name="answer" placeholder="Введите правильный ответ"></textarea>
	<h2> Варианты ответов </h2>
	<i> Варианты ответов необходимо вводить через разделительный символ ";" </i> <br><br>
	<textarea cols="40" rows="8" name="option" placeholder="Введите варианты ответов"></textarea>
	<br><br>
	<input type="submit" value="Добавить вопрос" />
</form>

<?php die; ?>
<?php endif; ?>


<?php
include ("function.php"); // подключаем функции

// определяем id для нового вопроса, по количеству уже имеющихся
$file = file_get_contents("test/test.json");
$test = json_decode($file);
$id = count($test) ;

//Создаем новый объект-вопрос
$newQuestion = new Question;
// Заполняем его данными
$newQuestion->id = $id + 1;
$newQuestion->text = clearStr($_POST['question']);
$newQuestion->answer = clearStr($_POST['answer']);

// Определяем наличие вариантов ответов если они есть, и формируем массив этих вариантов
if (!empty($_POST['option'])){
	$options = explode(";", clearStr($_POST['option']));
	$option = array();
	$error_spot = 0;

	foreach ($options as $value) {
		$var = new Option;
		$var->var = clearStr($value);
		if (clearStr($value) == clearStr($_POST['answer'])) {
			$var->status = "right";
			$error_spot++;
		}
		else {
			$var->status = "wrong";
		}
		$option[] = $var;	
	}
	// Если метка-ошибка не соответствует контрольному числу прирываем скрипт и просим повторить ввод данных по новой
	if($error_spot != 1){
		echo "Произошла ошибка при заполнении! <br><a href='create.php'> Попробовать ещё раз </a>";
		die;
	}
	// Дополняем объект-вопрос оставшимися данными
	$newQuestion->option = $option;
	$newQuestion->type = "radio";
}
else 
{
	// Дополняем объект-вопрос оставшимися данными
	$newQuestion->type = "input";
}

$test[$id] = $newQuestion;
file_put_contents("test/test.json", json_encode($test));

echo "Вопрос добавлен<br><a href='list.php'> Перейти к тесту </a>";


