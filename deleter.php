<?php
session_start();

if (!isset($_SESSION['id_status']) || $_SESSION['id_status'] != 1)
{
	http_response_code(403);
	echo "У вас нет прав, для выполнения этого действия!";
	die;
}

if(!isset($_POST['id']) || !file_exists("test/test.json"))
{
	echo "Невозможно выполнить действие<br><a href='list.php'> Перейти к тесту </a>";
	die;
}

$id = $_POST['id'];

$file = file_get_contents("test/test.json");
$test = json_decode($file);
$newtest = array();
$i = 0;

foreach ($test as $key => $value){
	if ($key == $id) continue;
	$newtest[$i] = $value;
	$newtest[$i]->id = $i + 1; 
	$i++;
}

file_put_contents("test/test.json", json_encode($newtest));

echo "Вопрос удален<br><a href='list.php'> Перейти к тесту </a>";