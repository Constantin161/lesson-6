<?php
// функция очищает строку от муссра
function clearStr($str){
  $str=trim($str);
  $str=strip_tags($str);
  $str=htmlspecialchars($str);
  $str=stripslashes($str);
  return $str;
}

// функция устанавливает параметр "disabled" для инпута
function disabled($id, $test, $spot){
	switch ($spot) {
		case '1':
			if($id==0){
				return "disabled";
			}
			break;
		case '2':
			if($id==count($test)-1){
				return "disabled";
			}
			break;
		default:
			break;
	}
}

// функция записывает данные в файл data.csv
function wrightData(/*$name,*/ $answers, $counter){
	$data = fopen("data/{$_SESSION['login']}/data.csv", "w");
	//fputs($data, $name."\n");
	fputs($data, $answers."\n");
	fputs($data, $counter);
	fclose($data);
}

class User
{
	public $login;
	public $pw;
}

class Question
{
	public $id;
	public $type;
	public $option;
	public $text;
	public $answer;
}

class Option
{
	public $var;
	public $status;
}