<?php
include('function.php');

//error_reporting(E_ALL);

session_start();

// Кусок для выхода из сессии
if (isset($_POST['exit']))
{
	//unset($_SERVER['PHP_AUTH_USER']);
	//unset($_SERVER['PHP_AUTH_PW']);
	session_destroy();
	header("Location: list.php");
	die;
}

if(isset($_SESSION['login'])): ?>
	<form method="post" action="index.php">
		<input type="hidden" name="exit" />
		<input type="submit" value="Выйти" />
	</form>
	<?php die; ?>
<?php endif;?>

<?php
if(!isset($_POST['login']) || !isset($_POST['pw']) ) { 
	
	//$_SESSION['login'] = '';
	//header('WWW-Authenticate: Basic realm="My_test_php"');
	//header('HTTP/1.0 401 Unauthorized');
	?>
	<h2>Вход в систему:</h2>
	<form method="post" action="index.php">
		<h3>Ваше имя</h3>
		<input type="text" name="login" placeholder="Введите ваше имя" required />
		<h3>Пароль</h3>
		<input type="password" name="pw" placeholder="Введите ваш пароль" required />
		<br><br>
		<input type="submit" value="Войти" />
	</form>
	<?php
	die;//("Извините, Вам отказано в доступе");
}

if($_POST['login']=="admin" && $_POST['pw']=="Rock&Roll")
// Если логин и пароль совпадают с администраторскими - приветствуем администратора
{
	echo "Приветствую, Администратор!";
	$_SESSION['id_status'] = 1;
}
else 
{
	$originalname = $_POST['login'];
	$name = md5(clearStr($originalname));
	$pw = md5(clearStr($_POST['pw']));
	if (!file_exists("user_data/{$name}.json")) // если не существует файла с данными пользователя 
	{
		// создаем файл .json с данными пользователя
		$user = new User;
		$user->login = $originalname;
		$user->pw = $pw;
		$user_file = fopen("user_data/{$name}.json", 'w');
		fwrite($user_file, json_encode($user));
		fclose($user_file);
	}
	else // если файл с даннымы существует
	{
		$user = json_decode(file_get_contents("user_data/{$name}.json"));
		if ($user->pw != $pw) die ("Извините, Вам отказано в доступе. Неверный пароль. <a href='index.php'>Назад</a>");
	}
	echo "Приветствую, {$_POST['login']}. Вы зашли как гость";
	$_SESSION['id_status'] = 2;
}
$_SESSION['is_authorized'] = 1;
$_SESSION['login'] = $_POST['login'];
?>
<br><br>
<a href="list.php"> Перейти к тесту </a>
