<?php
	session_start();
?>

<html>
<head>
	<title> Список вопросов </title>
	<meta charset="utf-8">
</head>
<body>
	<table>
	<tr>
		<td></td>
		<td></td>
			<?php 
			// если не авторизованы выводится форма
			if(!isset($_SESSION['is_authorized'])): ?>
				<form method="post" action="index.php">
				<td>
					<input type="text" name="login" placeholder="Введите ваше имя" required /> 
					<input type="password" name="pw" placeholder="Введите ваш пароль" required />
				</td>
				<td>
					<input type="submit" value="Войти" />
				</td>
				</form>
			<?php else: 
			// если авторизованы - приветствие
			?>
				<form method="post" action="index.php">
				<td>
					<b>Добрый день, <?= $_SESSION['login'] ?></b>
				</td>
				<td>
					<input type="hidden" name="exit" />
					<input type="submit" value="Выйти" />
				</td>
				</form>
			<?php endif; ?>
	</tr>
	<tr>
		<td></td>
		<td>
			<h3>Список вопросов</h3> 
		</td>
		<td>
			<?php // Если под админом - появляется возможность добавить вопрос
			if (isset($_SESSION['id_status']) && $_SESSION['id_status'] == 1): ?>
				<form action="creator.php" method="post">
					<input type="submit" value="Добавить вопрос в тест"/>
				</form>
			<?php endif; ?>
		</td>
	</tr>
	
	<?php
		$file = file_get_contents("test/test.json");
		$test = json_decode($file);
		
		// Получаем из файла data.csv значение строки с ответами для определения отвеченных
		if (isset($_SESSION['login']) && file_exists("data/{$_SESSION['login']}/data.csv"))
		{
			$data = fopen("data/{$_SESSION['login']}/data.csv", "r");
			//$answers = fgets($data);
			$answers = fgets($data);
			fclose($data);
		}
		else $answers = "";

		for ($i=0; $i < count($test); $i++): ?> 
		 	<tr>
		 		<td> 
					<?= $test[$i]->id ?>
		 		</td>
		 		<td>
					<a href= <?= "test.php?id={$test[$i]->id}" ?>> <?= $test[$i]->text ?> </a>
				</td>
				<td>
					<?php 
					$id = $test[$i]->id - 1;
					// Если ответ на вопрос дан (номер вопроса прописан в data.csv), то отображаем на странице "Ответ дан"
					if (mb_strpos($answers, "?".$id, NULL, "utf-8") !== false):?>  
						<i>Ответ дан</i>
					<?php endif; ?>
				</td>
		 		<td>
					<?php 
					// Если под админом - появляется возможность удалить вопрос
					if (isset($_SESSION['id_status']) && $_SESSION['id_status'] == 1): ?>
						<form action="deleter.php" method="post">
							<input type="submit" value="Удалить"/>
							<input type="hidden" name="id" value="<?= $test[$i]->id - 1 ?>" />
						</form>
					<?php endif; ?>
				</td>				
			</tr>
		<?php endfor ?>
	</table>
	<?php 
		// Если тест уже начат - выводим кнопки Завершить тест и Начать заново
		if (isset($_SESSION['login']) && file_exists("data/{$_SESSION['login']}/data.csv")): ?>
		<table>
			<tr>
				<td>
					<form id="next" method="post" action="yourresult.html" >
						<input type="submit" value="Завершить тест" />
					</form>
				</td>	
				<td>
					<form action="test.php" method="post">
						<input type="submit" value="Пройти заново"/>
						<input type="hidden" name="reboot" value="reboot" />
					</form>
				</td>
			</tr>
		</table>
	<?php endif; ?> 

	<?php
	// Если права администратора - подключаем форму загрузки файла теста
	 if (isset($_SESSION['id_status']) && $_SESSION['id_status'] == 1): ?> 
		<h3>Загрузка нового теста</h3>
		<form method="post" action="upload.php" enctype="multipart/form-data">
			<b>Загружаемый файл с тестом должен иметь формат json</b>
			<br><br>
			<input type="file" name="filename"/>
			<br><br>
			<input type="submit" value="Загрузить"/>
		</form>
		<?php
		if(isset($_GET["message"])){
		echo $_GET["message"];
		} ?>
	 <?php endif; ?> 
</body>
</html>
