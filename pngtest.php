<?php
	header("Content-Type: image/png");

	session_start();
	// Если не начали проходить тест (т.е не создан файл данных) страница не показывается
	if (!file_exists("data/{$_SESSION['login']}/data.csv") || !$_SESSION['is_authorized'])
	{
		http_response_code(404);
		//header("HTTP/1.1 404 Not Found");
		echo "<h1>Page Not Found</h1>";
		die;
	}

	$name = $_GET["name"];
	$result = $_GET["result"];
	$counter = $_GET["counter"];
	$answers = $_GET["answers"];
	/*$name = isset($_POST["name"]) ? $_POST["name"] : "";
	$counter = $_POST["counter"];
	$answers = $_POST["answers"];
	$answers = count(explode("?", $answers))-1;*/

	$img = imagecreatetruecolor(600, 700); //or die('Невозможно инициализировать GD поток');;
	$fc = imagecolorallocate($img, 255, 235, 0);
	$bc = imagecolorallocate($img, 255, 255, 0);
	$tc = imagecolorallocate($img, 215, 99, 130);

	// Рисуем сертификат
	imagefill($img, 0, 0, $bc);
	imagefilledpolygon($img, array(20, 20, 580, 20, 580, 680, 20, 680), 4, $fc);
	imagefilledpolygon($img, array(40, 40, 560, 40, 560, 660, 40, 660), 4, $bc);
	imagefilledpolygon($img, array(60, 60, 540, 60, 540, 640, 60, 640), 4, $fc);
	imagefilledpolygon($img, array(80, 80, 520, 80, 520, 620, 80, 620), 4, $bc);
	imagefilledellipse($img, 590, 690, 205, 205, $fc);
	imagefilledellipse($img, 590, 690, 200, 200, $bc);
	imagefilledellipse($img, 590, 690, 175, 175, $fc);
	imagefilledellipse($img, 590, 690, 170, 170, $bc);
	imageellipse($img, 590, 690, 190, 190, $fc);
	
	//Выводим надписи
	$font = realpath(__DIR__."/Monotype-Corsiva-Regular.ttf");
	
	$text = "Сертификат";
	$textbox = imageftbbox(30, 0, $font, $text);
	$x = 300 - $textbox[2] / 2;
	imagefttext($img, 30, 0, $x, 100, $tc, $font, $text);
	
	$text = "настоящий сертификат удостоверяет, что:";
	$textbox = imageftbbox(15, 0, $font, $text);
	$x = 300 - $textbox[2] / 2;
	imagefttext($img, 15, 0, $x, 140, $tc, $font, $text);
	
	$text = "$name";
	$textbox = imageftbbox(23, 0, $font, $text);
	$x = 300 - $textbox[2] / 2;
	imagefttext($img, 23, 0, $x, 200, $tc, $font, $text);

	$text = "прошел тест со следующими результатами:";
	imagefttext($img, 15, 0, 100, 260, $tc, $font, $text);

	$text = "- общее количество вопросов в тесте: {$number}";
	imagefttext($img, 15, 0, 100, 290, $tc, $font, $text);

	$text = "- количество данных ответов: {$answers}";
	imagefttext($img, 15, 0, 100, 310, $tc, $font, $text);

	$text = "- количество правильных ответов: {$counter}";
	imagefttext($img, 15, 0, 100, 330, $tc, $font, $text);

	$text = "- процент правильных ответов: {$result}%";
	imagefttext($img, 15, 0, 100, 350, $tc, $font, $text);
	
	imagepng($img);
	imagedestroy($img);
?>