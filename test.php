<?php

include("function.php"); // Подключаем файл дополнительных функций

session_start(); //стартуем сессию

// Если не авторизован пользователь отправляем на авторизацию
if(!$_SESSION['is_authorized']){
	header('Location: index.php');
	die;
}

// если нажата кнопка "Пройти заново" уничтожаем файл с данными и идем к списку
if(isset($_POST["reboot"]))
{
	unlink ("data/{$_SESSION['login']}/data.csv");
	header("Location: list.php");
	die;
}

// Получаем объект из файла
$file = file_get_contents("test/test.json");
$test = json_decode($file);

// Если директория не существует - создаем
if (!file_exists("data/{$_SESSION['login']}"))
{
	mkdir("data/{$_SESSION['login']}");
}

// Если файл не существует - создаем его
if (!file_exists("data/{$_SESSION['login']}/data.csv"))
{
	$data = fopen("data/{$_SESSION['login']}/data.csv", "w");
	//fputs($data, "0\n");
	fputs($data, "0\n");
	fputs($data, "0");
	fclose($data);
}

// добавить, что б создавалась папка с именем пользователя и в ней хранилась инфа

// считываем информацию из файлов, содержащих наши переменные
$data = fopen("data/{$_SESSION['login']}/data.csv", "r");
//$name = isset($_GET["name"])? $_GET["name"] : clearStr(fgets($data));
$answers = clearStr(fgets($data));
$counter = clearStr(fgets($data));
fclose($data);

//Устанавливаем значение переменной $id
$id = isset($_GET["id"]) ? $_GET["id"] - 1 : 0;

// Если запрашивается неверный номер вопроса
if($id < 0 or $id > (count($test)-1)){
	http_response_code(404);
	//header("HTTP/1.1 404 Not Found");
	echo "<h1>Page Not Found</h1>";
	die;
}

?>

<html>
<head>
	<title> Вопрос № <?= $test[$id]->id ?> </title>
	<meta charset="utf-8">
</head>
<body>
	<b>Добрый день, <?= $_SESSION['login'] ?></b>
	
	<?php 
	//if(empty($name)): ?>
	<!--	<h3> Введите своё имя </h3>
		<input type="text" name="name" form="form" placeholder="Введите своё имя"  /> -->
	<?php //endif ?>

	<h3> Вопрос № <?= $test[$id]->id ?> </h3>
	<b> <?= $test[$id]->text ?></b>
	<br><br>
	
	<?php  
	
	// Проверяем был дан ответ на данный вопрос раннее или нет, id вопросов хранятся в строке $answers 
	if(mb_strpos($answers, "?".$id, NULL, "utf-8") !== false){
		echo "<font color='red'> Вы отвечали на этот вопрос! </font><br><br>";
	}
	// Если ответ был дан  - проверяем его правильность.
	elseif(isset($_GET["answer"]))
	{
		$answer = clearStr($_GET["answer"]);
		$rightanswer = $test[$id]->answer;
		$answers .= "?".$id;
		if($answer==$rightanswer) $counter++; // Если ответ правильный увеличиваем счетчик правильных ответов
		echo "<font color='green'>Ваш ответ принят! </font><br><br>";

	}
	else include ('form.php');  // Загружаем форму вопроса
	
	wrightData(/*$name,*/ $answers, $counter); // записываем данные в файл data.csv
	
	?>
	
	<!-- Кнопки навигации по вопросам-->
	<table>
		<tr>
			<td>
				<form id="prev" method="get" action="test.php" >
					<input type="submit" value="Предыдущий вопрос" <?= disabled($id, $test, 1) ?> />
					<input type="hidden" name="id" value="<?= $id ?>" />
				</form>
			</td>
			<td>
				<form method="post" action="list.php">
					<input type="submit" value="К списку вопросов"/>
				</form>
			</td>
			<td>
				<form id="next" method="get" action="test.php">
					<input type="submit" value="Следующий вопрос" <?= disabled($id, $test, 2) ?> />
					<input type="hidden" name="id" value="<?= $id+2 ?>" />

				</form>
			</td>
		</tr>
		<tr>
			<td>
				<form id="next" method="post" action="yourresult.html" >
					<input type="submit" value="Завершить тест" />
				</form>
			</td>	
			<td>
				<form action="test.php" method="post">
					<input type="submit" value="Пройти заново"/>
					<input type="hidden" name="reboot" value="reboot" />
				</form>
			</td>
			<td>	
			</td>
		</tr>
	</table>
</body>
</html>