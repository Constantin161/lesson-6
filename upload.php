<?php

session_start();

if (!isset($_SESSION['id_status']) || $_SESSION['id_status'] != 1)
{
	http_response_code(403);
	echo "У вас нет прав, для выполнения этого действия!";
	die;
}

include("function.php");

// Если разрешение не подходит сообщаем об этом
if(!fnmatch("*.json", $_FILES["filename"]["name"])){
	$message = "Вы попытались загрузить файл в формате, отличном от json";
	header("Location: list.php?message=$message");
	die;
}

if(is_uploaded_file($_FILES["filename"]["tmp_name"])){
	move_uploaded_file($_FILES["filename"]["tmp_name"], "test/test.json");
	header("Location: list.php");
	die;
}
else {
	$message = "Не удалось загрузить файл";
	header("Location: list.php?message=$message");
	die;
}

